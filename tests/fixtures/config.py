from pathlib import Path

import pytest
import ruamel.yaml

from ktdg.generation import Config, _add_comments
from ktdg import answers, questions, skills, students


@pytest.fixture
def config() -> Config:
    config = Config()
    config.skills.n = 3  # type: ignore
    config.students.n = 5  # type: ignore
    config.questions.n = 10  # type: ignore
    return config


@pytest.fixture
def config_file(tmp_path: Path, config: Config) -> str:
    path = tmp_path / "test.yml"
    yaml = ruamel.yaml.YAML()
    yaml.dump(_add_comments(config), path)
    return str(path)


@pytest.fixture
def skills_config(config: Config) -> skills.Config:
    return config.skills  # type: ignore


@pytest.fixture
def students_config(config: Config) -> students.Config:
    return config.students  # type: ignore


@pytest.fixture
def questions_config(config: Config) -> questions.Config:
    return config.questions  # type: ignore


@pytest.fixture
def answers_config(config: Config) -> answers.Config:
    return config.answers
