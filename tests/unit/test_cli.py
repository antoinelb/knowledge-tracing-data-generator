from pathlib import Path

import pytest
from pytest_mock import MockerFixture
from typer import Typer
from typer.testing import CliRunner

from ktdg.cli import init_cli, run_cli
from ktdg.generation import read_config


def test_cli(runner: CliRunner, cli: Typer) -> None:
    resp = runner.invoke(cli, ["-h"])
    assert resp.exit_code == 0
    assert resp.output.startswith("Usage")

    resp = runner.invoke(cli, ["--help"])
    assert resp.exit_code == 0
    assert resp.output.startswith("Usage")


def test_init_cli() -> None:
    cli = init_cli()
    assert isinstance(cli, Typer)


def test_run_cli(mocker: MockerFixture) -> None:
    init_cli = mocker.patch("ktdg.cli.init_cli")
    run_cli()
    init_cli.assert_called_once()


@pytest.mark.parametrize("command", ["create", "c"])
def test_create_or_update(
    runner: CliRunner, cli: Typer, tmp_path: Path, command: str
) -> None:
    path = tmp_path / "test.yml"
    assert not path.exists()
    resp = runner.invoke(cli, [command, str(path)])
    assert resp.exit_code == 0
    assert "Created" in resp.output
    assert path.exists()
    read_config(path)

    resp = runner.invoke(cli, [command, str(path)])
    assert resp.exit_code == 0
    assert "Updated" in resp.output

    resp = runner.invoke(cli, [command, str(path)[:-4]])
    assert resp.exit_code == 0
    assert ".yml" in resp.output


@pytest.mark.parametrize("command", ["generate", "g"])
def test_generate(
    runner: CliRunner,
    cli: Typer,
    tmp_path: Path,
    config_file: str,
    command: str,
) -> None:
    assert not (tmp_path / "test.json").exists()
    resp = runner.invoke(
        cli, [command, config_file, str(tmp_path / "test.json")]
    )
    assert resp.exit_code == 0
    assert (tmp_path / "test.json").exists()
