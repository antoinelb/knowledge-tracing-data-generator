import ruamel.yaml

from ktdg.distributions import Normal
from ktdg.skills import (
    Config,
    add_comments,
    generate,
)

############
# external #
############


def test_add_comments(skills_config: Config) -> None:
    config_ = add_comments(skills_config)
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert len(config_.ca.items) == 4
    assert len(config_) == 4
    assert "n" in config_.ca.items
    assert "difficulty" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items
    assert "difficulty" in config_


def test_generate(skills_config: Config) -> None:
    skills_config.difficulty = Normal(mu=0.5, sigma=0.25)

    skills = generate(skills_config)
    assert len(skills) == skills_config.n
    assert all(s["id"] == i for i, s in enumerate(skills))
    assert all(0 <= s["difficulty"] <= 1 for s in skills)
    assert all(s["hash"] == skills_config.hash for s in skills)

    skills_ = generate(skills_config)
    assert skills == skills_
