import json
import re
from pathlib import Path

from pytest_mock import MockerFixture
import ruamel.yaml
import pytest

from ktdg.generation import (
    Config,
    Data,
    _add_comments,
    generate,
    read_config,
    save_config,
    save_data,
)
from tests.utils import equals


############
# external #
############


def test_read_config(config_file: str) -> None:
    config = read_config(Path(config_file))
    assert isinstance(config, Config)


def test_read_config__doesnt_exist(tmp_path: Path) -> None:
    with pytest.raises(FileNotFoundError):
        read_config(tmp_path / "test.yml")


def test_save_config(tmp_path: Path, config: Config) -> None:
    path = tmp_path / "config" / "config.yml"
    assert not path.exists()
    save_config(config, path)
    with open(path) as f:
        for i, line in enumerate(f):
            if not any(
                line.strip().startswith(key)
                for key in ("skills", "questions", "students", "answers", "")
            ):
                assert (
                    re.search(r"# .+$", line) is not None
                ), f"Line {i} - {line:!r} is missing a comment."


def test_generate(config: Config) -> None:
    data = generate(config)
    assert set(data.keys()) == set(Data.__annotations__.keys())
    assert len(data["skills"]) == config.skills.n  # type: ignore
    assert len(data["students"]) == config.students.n  # type: ignore
    assert len(data["questions"]) == config.questions.n  # type: ignore
    assert len(data["answers"]) > 0


def test_save_data(mocker: MockerFixture, tmp_path: Path, data: Data) -> None:
    mocker.patch("ktdg.generation.Path", return_value=tmp_path)
    path = tmp_path / "data.json"
    assert not path.exists()
    save_data(data, path)
    assert path.exists()
    with open(path) as f:
        data_ = json.load(
            f,
            object_hook=lambda d: {
                int(k) if k.isdigit() else k: v for k, v in d.items()
            },
        )
    assert equals(data, data_)


############
# internal #
############


def test_add_comments(config: Config) -> None:
    config_ = _add_comments(config)
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert len(config_) == 5
    assert "skills" in config_
    assert "questions" in config_
    assert "students" in config_
    assert "answers" in config_
    assert "hash" in config_
