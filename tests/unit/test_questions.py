import ruamel.yaml

from ktdg import Skill
from ktdg.distributions import Normal
from ktdg.questions import (
    Config,
    add_comments,
    _determine_difficulty_adjustment,
    generate,
)

############
# external #
############


def test_add_comments(questions_config: Config) -> None:
    config_ = add_comments(questions_config)
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert len(config_.ca.items) == 8
    assert len(config_) == 8
    assert "n" in config_.ca.items
    assert "n_skills" in config_.ca.items
    assert "skill_mastery" in config_.ca.items
    assert "difficulty" in config_.ca.items
    assert "slip" in config_.ca.items
    assert "guess" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items
    assert "n_skills" in config_
    assert "skill_mastery" in config_
    assert "difficulty" in config_
    assert "slip" in config_
    assert "guess" in config_


def test_generate(questions_config: Config, skills: list[Skill]) -> None:
    questions_config.n_skills = Normal(mu=3, sigma=2)
    questions_config.skill_mastery = Normal(mu=0.25, sigma=0.25)
    questions_config.difficulty = Normal(mu=0.5, sigma=0.25)
    questions_config.slip = Normal(mu=0.1, sigma=0.2)
    questions_config.guess = Normal(mu=0.1, sigma=0.2)

    questions = generate(questions_config, skills)
    assert all(q["id"] == i for i, q in enumerate(questions))
    assert len(questions) == questions_config.n
    assert all(0 <= q["difficulty"] <= 1 for q in questions)
    assert all(0 <= q["slip"] <= 1 for q in questions)
    assert all(0 <= q["guess"] <= 1 for q in questions)
    assert all(
        len(q["skills"]) > 0 and all(0 <= m <= 1 for m in q["skills"].values())
        for q in questions
    )
    assert all(q["hash"] == questions_config.hash for q in questions)

    questions_ = generate(questions_config, skills)
    assert questions == questions_


############
# internal #
############


def test_determine_difficulty_adjustment(skills: list[Skill]) -> None:
    skills = [
        {
            **skill,  # type: ignore
            "difficulty": 1 + (1 if i % 2 == 0 else -1) * 0.5,
        }
        for i, skill in enumerate(skills)
    ]

    skills_ = [0, 1, 2]
    masteries = [0.2 * i for i in range(len(skills_))]

    correct = 1 * 0.9 * 1.2

    assert (
        _determine_difficulty_adjustment(skills, skills_, masteries) == correct
    )
