import io
from typing import Any

import numpy as np
from _pytest.capture import CaptureFixture
from pydantic import BaseModel
from tqdm import tqdm

from ktdg.utils import (
    _hash_config,
    _prepare_config_for_hashing,
    clip_0_1,
    create_skill_vector,
    done_print,
    hash_config,
    load_print,
    load_progress,
    parse_config,
    set_field,
    set_seed_if_missing,
)

############
# external #
############


def test_load_print(capsys: CaptureFixture) -> None:
    load_print("test")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[*]" in output
    assert "test" in output
    assert not output.endswith("\n")
    assert output.index("[*]") < output.index("test")

    load_print("test", "1/2")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[1/2]" in output
    assert "[*]" not in output
    assert "test" in output
    assert not output.endswith("\n")
    assert output.index("[1/2]") < output.index("test")

    load_print("test", echo=False)
    assert not capsys.readouterr().out


def test_done_print(capsys: CaptureFixture) -> None:
    done_print("test")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[+]" in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[+]") < output.index("test")

    done_print("test", "1/2")
    output = capsys.readouterr().out
    assert output.startswith("\r")
    assert "[1/2]" in output
    assert "[+]" not in output
    assert "test" in output
    assert output.endswith("\n")
    assert output.index("[1/2]") < output.index("test")
    done_print("test", echo=False)
    assert not capsys.readouterr().out


def test_load_progress() -> None:
    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", file=output_)
        output = output_.getvalue()
        assert isinstance(iter_, tqdm)
        assert "[*]" in output
        assert "test" in output
        assert output.index("[*]") < output.index("test")

    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", "1/2", file=output_)
        output = output_.getvalue()
        assert isinstance(iter_, tqdm)
        assert "[1/2]" in output
        assert "[*]" not in output
        assert "test" in output
        assert output.index("[1/2]") < output.index("test")

    with io.StringIO() as output_:
        iter_ = load_progress(range(10), "test", echo=False, file=output_)
        output = output_.getvalue()
        assert not isinstance(iter_, tqdm)


def test_set_field() -> None:
    class A(BaseModel):
        a: str = "a"

        _parse_a = set_field("a", "b")

    a = A()
    assert a.a == "b"

    a = A(a="c")
    assert a.a == "b"


def test_set_seed_if_missing() -> None:
    class A(BaseModel):
        a: int = 0

        _parse_a = set_seed_if_missing("a")

    assert isinstance(A().a, int)
    assert isinstance(A(a=0).a, int)
    for _ in range(10):
        assert A(a=123).a == 123


def test_hash_config() -> None:
    class A(BaseModel):
        a: str
        b: int
        hash: str = ""

        _hash_config = hash_config()

    a = A(a="a", b=1)
    assert a.hash != ""

    b = A(b=1, a="a")
    assert b.hash == a.hash

    c = A(a="a", b=1, hash="abc")
    assert c.hash == a.hash

    d = A(a="b", b=1)
    assert d.hash != a.hash


def test_parse_config() -> None:
    class B(BaseModel):
        a: str = "a"
        b: int = 1

    def parse_config_(config: dict[str, Any] | None = None) -> B:
        if config is None:
            return B()
        elif config["a"] == "a":
            return B(**config)
        else:
            return B(**{**config, "b": 2})

    class A(BaseModel):
        a: B | None = parse_config_()

        _parse_a = parse_config("a", parse_config_)

    a = A()
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a=None)
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a={"a": "a", "b": 1})
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 1  # type: ignore

    a = A(a={"a": "a", "b": 3})
    assert a.a.a == "a"  # type: ignore
    assert a.a.b == 3  # type: ignore

    a = A(a={"a": "b", "b": 3})
    assert a.a.a == "b"  # type: ignore
    assert a.a.b == 2  # type: ignore

    a = A(a=B(a="b", b=3))
    assert a.a.a == "b"  # type: ignore
    assert a.a.b == 3  # type: ignore


def test_clip_0_1() -> None:
    values = [-1, 0, 0.5, 0.99, 1, 2]
    assert clip_0_1(values) == [0, 0, 0.5, 0.99, 1, 1]


def test_create_skill_vector() -> None:
    skills = {i: i / 10 for i in range(0, 10, 2)}
    n_skills = 10

    correct = np.array([0, 0, 0.2, 0, 0.4, 0, 0.6, 0, 0.8, 0])

    assert (create_skill_vector(skills, n_skills) == correct).all()


############
# internal #
############


def test_hash_config_() -> None:
    config = {"a": 1, "c": 3, "b": 2}
    assert _hash_config(config) == _hash_config(config)
    assert _hash_config(config) == _hash_config(
        {k: config[k] for k in sorted(config)}
    )
    assert _hash_config(config) != _hash_config({"a": 1})


def test_prepapre_config_for_hashing() -> None:
    config = {"a": [1, {"hash": ""}], "c": 3, "b": {"hash": ""}, "hash": ""}
    correct = {"a": [1, {}], "b": {}, "c": 3}
    assert _prepare_config_for_hashing(config) == correct
