import ruamel.yaml

from ktdg import Skill
from ktdg.distributions import Normal
from ktdg.students import Config, add_comments, generate

############
# external #
############


def test_add_comments(students_config: Config) -> None:
    config_ = add_comments(students_config)
    assert isinstance(config_, ruamel.yaml.CommentedMap)
    assert len(config_.ca.items) == 10
    assert len(config_) == 10
    assert "n" in config_.ca.items
    assert "binary_learning" in config_.ca.items
    assert "n_skills" in config_.ca.items
    assert "skill_mastery" in config_.ca.items
    assert "guess" in config_.ca.items
    assert "slip" in config_.ca.items
    assert "learning_rate" in config_.ca.items
    assert "forget_rate" in config_.ca.items
    assert "seed" in config_.ca.items
    assert "hash" in config_.ca.items
    assert "n_skills" in config_
    assert "skill_mastery" in config_
    assert "guess" in config_
    assert "slip" in config_
    assert "learning_rate" in config_
    assert "forget_rate" in config_


def test_generate(students_config: Config, skills: list[Skill]) -> None:
    students_config.n_skills = Normal(mu=3, sigma=2)
    students_config.skill_mastery = Normal(mu=0.25, sigma=0.25)
    students_config.slip = Normal(mu=0.1, sigma=0.2)
    students_config.guess = Normal(mu=0.1, sigma=0.2)
    students_config.learning_rate = Normal(mu=0.1, sigma=0.2)
    students_config.forget_rate = Normal(mu=0.1, sigma=0.2)

    students = generate(students_config, skills)
    assert all(s["id"] == i for i, s in enumerate(students))
    assert len(students) == students_config.n
    assert all(0 <= s["slip"] <= 1 for s in students)
    assert all(0 <= s["guess"] <= 1 for s in students)
    assert all(0 <= s["learning_rate"] <= 1 for s in students)
    assert all(0 <= s["forget_rate"] <= 1 for s in students)
    assert all(
        len(s["skills"]) > 0 and all(0 <= m <= 1 for m in s["skills"].values())
        for s in students
    )
    assert all(s["hash"] == students_config.hash for s in students)

    students_ = generate(students_config, skills)
    assert students == students_
